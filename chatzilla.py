activate_this = '/home/myusername/.virtualenvs/flaskchat/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from gevent import monkey
from flask import Flask, Response, render_template, request
from socketio import socketio_manage
from socketio.namespace import BaseNamespace
from socketio.mixins import BroadcastMixin
from time import time
import re, random

monkey.patch_all()

application = Flask(__name__)
application.debug = True
application.config['PORT'] = 8080


class ChatNamespace(BaseNamespace, BroadcastMixin):
    
    stats = {
        "people" : []
    }

    dice_re = re.compile('(\d*)(?:d|D)(\d*)')

    def initialize(self):
        self.logger = application.logger
        self.log("Socketio session started")
        # pass

    def log(self, message):
        self.logger.info("[{0}] {1}".format(self.socket.sessid, message))
        # pass

    def report_stats(self):
        self.broadcast_event("stats",self.stats)
        # pass

    def recv_connect(self):
        self.log("New connection")
        # pass

    def recv_disconnect(self):
        self.log("Client disconnected")
        # pass
        
        if self.session.has_key("email"):
            email = self.session['email']

            self.broadcast_event_not_me("debug", "%s left" % email)
            
            self.stats["people"] = filter(lambda e : e != email, self.stats["people"])
            self.report_stats()

    def on_join(self, email):
        self.log("%s joined chat" % email)
        self.session['email'] = email

        if not email in self.stats["people"]:
            self.stats["people"].append(email) 

        self.report_stats()

        return True, email

    def on_message(self, message):
        message_data = {
            "sender" : self.session["email"],
            "content" : message,
            "sent" : time()*1000 #ms
        }

        if message[:5] == '/roll':
            try:
                rollo = self.roll(message)
            except Exception, e:
                rollo = 'Error in formula!\n' + str(e)
            message_data['content'] = message = rollo

        self.broadcast_event_not_me("message",{ "sender" : self.session["email"], "content" : message})
        return True, message_data

    def roll(self, roll_string):
        output = []
        sta = []
        add = []
        sub = []
        end = []

        explode = False

        spli = roll_string.split(' ')[1:]

        for n, i in enumerate(spli):
            if i == 'explode':
                explode = True
                sta.append('explode')

            elif 'd' in i or 'D' in i:
                numr, dic = self.dice_re.search(i).groups()
                dic = int(dic)

                sta.append(i)

                try:
                    numr = range(int(numr))
                except ValueError:
                    numr = 0

                dicerolls = []

                for j in numr or range(1):
                    dicerolls.append(random.randint(1, dic))

                if explode:
                    while dic in dicerolls:
                        expl = [
                                (m, random.randint(1, dic)) 
                                for m, j in enumerate(dicerolls)
                                if j == dic
                                ]
                        for m, j in reversed(expl):
                            dicerolls[m] = 'e' + str(dicerolls[m])
                            dicerolls.insert(m+1, j)

                output += dicerolls

            elif i[0] == '+':
                if len(i) > 1:
                    add.append(int(i[1:]))
                else:
                    add.append(int(spli[n+1]))

            elif i[0] == '-':
                if len(i) > 1:
                    sub.append(int(i[1:]))
                else:
                    sub.append(int(spli[n+1]))

            elif i[0] == '#':
                end = spli[n:]
                break

        total = 0
        for i in output:
            try:
                total += int(i)
            except ValueError:
                total += int(i[1:])
        total += sum(add) - sum(sub)

        return (
                ' - '.join(
                           [' + '.join(map(str, sta + add))] + 
                           map(str, sub)
                           ) + 
                ' = ' + 
                ' - '.join(
                           [' + '.join(map(str, output + add))] + 
                           map(str, sub)
                           ) + 
                ' = ' + str(total) + ' ' + ' '.join(end)
                )


@application.route('/', methods=['GET'])
def landing():
    return render_template('landing.html')

@application.route('/logs', methods=['GET'])
def view_logs():
    return render_template('log.html')

@application.route('/socket.io/<path:remaining>')
def socketio(remaining):
    try:
        socketio_manage(request.environ, {'/chat': ChatNamespace}, request)
        print('Correctly handled')
    except:
        application.logger.error("Exception while handling socketio connection",
                                 exc_info=True)
        print('Error passed')
    return Response()

# if __name__ == '__main__':
#     ip = ''
#     port = 8080
#     SocketIOServer((ip, port), application).serve_forever()